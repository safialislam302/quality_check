import gitlab
import numpy as np
import pandas as pd

Object_list = []
Int_list = []
Float_list = []

col_row_num = [] # row column number
non_value_check = [] # None value check
missing_cell_info = [] # Missing cell information details
missing_header = []

column_unique = [] # unique column check (two columns are same or not)
column_empty = [] #empty column check
column_same_value = [] # all the column values are same or not (particular one column)
column_nan_value_count = []
column_zero_value_count = []

row_empty = []
row_unique = []
row_unique_count = []
row_unique_count_percentage = []

def gitlab_connection():
    """Establish GitLab connection and get project"""
    auth_token = 'glpat-qPafdzQkdLUNhonux6hJ'
    git_connect = gitlab.Gitlab('https://gitlab.com/', private_token=auth_token)
    git_connect.auth()
    project_list = git_connect.projects.list(owned=True)
    return git_connect, project_list

def email_file_check(name):
    """Check email file is present or not"""
    author_list = []
    if name == "Email.txt":
        data_files = 'Upload/'
        author_name = name
        with open(f"{data_files}{author_name}") as f:
            author_list = [line.rstrip() for line in f]
    return author_list

def list_of_file(name):
    """Search for the file name in the upload directory"""
    data_files = 'Upload/'
    file_name = name
    with open(f'{data_files}{file_name}') as file_temp:
        file_list = [line.rstrip() for line in file_temp]
    return file_list

def author_present_check(name, author_present):
    """Check author file is present or not"""
    if name == "authors.md" or name == "Authors.md" or name == "AUTHORS.md" or name == "authors.txt" or name == "Authors.txt" or name == "AUTHORS.txt":
        author_present = 1
    return author_present

def license_present_check(name, license_present):
    """Check license file is present or not"""   
    if name == "license.md" or name == "License.md" or name == "LICENSE.md" or name == "license.txt" or name == "License.txt" or name == "LICENSE.txt":
        license_present = 1
    return license_present

def readme_present_check(name, readme_present):
    """Check readme file is present or not"""
    if name == "readme.md" or name == "Readme.md" or name == "README.md" or name == "readme.txt" or name == "Readme.txt" or name == "README.txt":
        readme_present = 1
    return readme_present

def type_check(data):
    """Check each column have more than 2 types of variables"""
    object_num = list(data.select_dtypes(include='object'))
    int_num = list(data.select_dtypes(include='int64'))
    float_num = list(data.select_dtypes(include='float64'))
    return object_num, int_num, float_num

def duplicate_column_check(data, name):
    """Duplicate column check"""
    for col in data.columns:    
        for col_2 in data.columns:
            if col == col_2:
                continue
            else:
                column_temp = data[col].equals(data[col_2])
                if column_temp == True:
                    column_check = "File name: " + name + " column name: " + col_2
                    column_unique.append(column_check)
    return column_unique

def row_unique_check(data, name):
    """Duplicate Row check"""
    for row_one in range(0, data.shape[0]):
        for row_all in range(0, data.shape[0]):
            if row_one == row_all:
                continue
            else:
                data1 = data.values[row_one].tolist()
                x_data = [element for element in data1 if str(element) != "nan"]
                x_data = np.array(x_data)
                data2 = data.values[row_all].tolist()
                y_data = [element for element in data2 if str(element) != "nan"]
                y_data = np.array(y_data) 
                row_equal = np.array_equal(x_data, y_data)            
                if row_equal == True:
                    row_check = "File name: " + name + " row number: " + str(row_all + 2)
                    row_unique.append(row_check)
    return row_unique

def column_empty_check(data, name):
    """Full empty column check"""
    col_number = 1
    for col in data.columns:  
        c_series = pd.isnull(data[col])
        c_list = c_series.tolist()
        c_list_len = len(c_list)
        c_check_nan = [element for element in c_list if str(element) == 'True']
        c_check_nan_len = len(c_check_nan)
        if c_check_nan_len == c_list_len:
            c_empty = "File name: " + name + " Column number " +  str(col_number) + " column name: " + col + " are full empyty"
            column_empty.append(c_empty)
        col_number = col_number + 1
    return column_empty

def row_empty_check(data, name):
    """Full empty row check"""
    row_number = 1
    for row_one in range(0, data.shape[0]):
        r_list = data.values[row_one].tolist()
        r_list_len = len(r_list)
        r_check = [element for element in r_list if str(element) == "nan"]
        r_check_len = len(r_check)
        if r_check_len == r_list_len:
            r_empty = "File name: " + name + " Row number " + str(row_number) + " are full empyty."
            row_empty.append(r_empty)
        row_number = row_number + 1
    return row_empty

def column_same_value_check(data, name):
    """All column values are same"""
    for col in data.columns:
        col_array = data[col].unique()
        col_list = col_array.tolist()
        if len(col_list) <= 1:
            col_list_new = [x for x in col_list if pd.isnull(x) == False and x != 'nan']
            if len(col_list_new) == 1:
                column_same = "File name: " + name + " Column name: " + col + " all values are same."
                column_same_value.append(column_same)
    return column_same_value

def missing_cell_count_check(data, name, col_temp, cells_count):
    """Empty value check"""
    cols_new = data.columns
    for i in cols_new.values:
        row_temp = 1
        for j in data[i]:
            if (pd.isna(j)):
                non_value = "In " + name + " Nan Values found in row: " + str(row_temp + 1) + " in column: " + str(i)
                non_value_check.append(non_value)
                cells_count = cells_count + 1
            row_temp = row_temp + 1
    col_temp = col_temp + 1
    return non_value_check, cells_count

def missing_header_check(data, index, name):
    """Missing Header check"""
    for i in data.axes[1]:
        if i.find("Unnamed:") == 0:
            missing_header_absent = "In " + name + " Header of Column " + str(index) + " is Absent." 
            missing_header.append(missing_header_absent)
        index = index + 1
    return missing_header

def column_nan_zero_value_count(data, name):
    """Column wise nan values and zero values count"""
    cols_new = data.columns
    # Column wise missing values count
    for col in cols_new:
        if data[col].isna().sum() != 0:
            column_nan_value = "File name: " + name +   " Column name: " + col + "\t missing values: " + str(data[col].isna().sum()) 
            column_nan_value_count.append(column_nan_value)                
    # Column wise zero values count
    for col in cols_new:
        if (data[col] == 0).sum() != 0:
            column_zero_value = "File name: " + name + " Column name: " + col + "\t zero values: " +  str((data[col] == 0).sum())
            column_zero_value_count.append(column_zero_value)
    return column_nan_value_count, column_zero_value_count

def variable_type_check(data, object_num):
    """Check each column have more than 2 types of variables"""
    data_copy = data.copy()
    for i in object_num:
        check1 = data_copy[i].str.match('[0-9]{1}')
        check2 = list(check1.unique())
        if True in check2 and False in check2:
            print(f'Coloumn {i} hase two or more different types of variables.')
    print("\n")

def list_to_str(list_info):
    """convert list to string"""
    return '\n'.join([ str(element) for element in list_info])

def main():
    """main function"""
    readme = 0
    license = 0
    author = 0
    git_connect, project_list = gitlab_connection()
    for i in project_list:           
        project = git_connect.projects.get(i.id)
        items = project.repository_tree(path='Upload') 
        for i in items:
            author_list = email_file_check(i['name'])
            # Search for the file name in the upload directory 
            if i['name'] == 'File.txt':
                file_list = list_of_file(i['name'])
                for j in range(len(file_list)):   
                    author_present = author_present_check(file_list[j], author)
                    license_present = license_present_check(file_list[j], license)
                    readme_present = readme_present_check(file_list[j], readme)
                    if file_list[j][-4:] == ".csv":
                        data_files = 'Upload/'
                        csv_name = file_list[j]
                        data = pd.read_csv(f"{data_files}{csv_name}")
                        if data.size > 0:
                            print("CSV file is openable\n")  

                        col_row_info = "File name: " + csv_name + " Row count: " + str(len(data)) + " column count: " + str(len(data.columns))
                        col_row_num.append(col_row_info)

                        # Column wise nan values and zero values count
                        column_nan_value_count, column_zero_value_count = column_nan_zero_value_count(data, csv_name)
     
                        # Empty value check
                        col_temp = 1
                        cells_count = 1
                        total_cells = len(data.columns) * len(data)
                        non_value_check, missing_cells_count = missing_cell_count_check(data, csv_name, col_temp, cells_count)
                        missing_cell = "File name: " + csv_name + " Total cells: " + str(total_cells) + " missing cells: " + str(missing_cells_count) + " missing cells percentage: " + str(round((missing_cells_count/total_cells) * 100, 2)) + "%"
                        missing_cell_info.append(missing_cell)

                        # Missing Header check
                        index = 1
                        missing_header = missing_header_check(data, index, csv_name)

                        # Type check
                        object_num, int_num, float_num = type_check(data)
                        Object_list.append(object_num)
                        Int_list.append(int_num)
                        Float_list.append(float_num)

                        # Check each column have more than 2 types of variables.
                        variable_type_check(data, object_num)

                        # Duplicate column check
                        column_unique = duplicate_column_check(data, csv_name)

                        # Duplicate Row, Row count and percentage
                        row_count = "File name: " + csv_name + " dupliate row count: " + str(data.duplicated().sum())
                        row_unique_count.append(row_count)
                        row_percentage = "File name: " + csv_name + " percentage of dupliate row : " + str(round((data.duplicated().sum() / len(data)) * 100, 2)) + "%"
                        row_unique_count_percentage.append(row_percentage)
                        row_unique = row_unique_check(data, csv_name)

                        # Full empty row and column check
                        column_empty = column_empty_check(data, csv_name)
                        row_empty = row_empty_check(data, csv_name)

                        # All column values are same
                        column_same_value = column_same_value_check(data, csv_name)

    # Convert list to string
    col_row_num_str = list_to_str(col_row_num)
    non_value_str = list_to_str(non_value_check)
    missing_cell_info_str = list_to_str(missing_cell_info)
    column_unique_str = list_to_str(column_unique)    
    row_unique_str = list_to_str(np.unique(row_unique))
    row_unique_count_str = list_to_str(np.unique(row_unique_count))
    row_unique_count_percentage_str = list_to_str(np.unique(row_unique_count_percentage))
    column_empty_str = list_to_str(np.unique(column_empty))
    row_empty_str = list_to_str(np.unique(row_empty))
    column_same_value_str = list_to_str(np.unique(column_same_value))
    column_nan_value_count_str = list_to_str(np.unique(column_nan_value_count))
    column_zero_value_count_str = list_to_str(np.unique(column_zero_value_count))
    missing_header_str = list_to_str(missing_header)

    if author_present == 1:
        author_str = "Author file present"
    else:
        author_str = "Author file not present"  
    if license_present == 1:
        license_str = "License file present"
    else:
        license_str = "License file not present"
    if readme_present == 1:
        readme_str = "Readme file present"
    else:
        readme_str = "Readme file not present"
    body = """Columns and Row number: \n%s \n\nMissing Values: \n%s \n\n\n%s \n\n%s \n\n%s \n\n%s \n\nObject type variables are: %s \n\nInteger type variables are: %s \n\nFloat type variables are: %s \n\n\nEmpty Cells information: \n%s \n\n\nNan Value Check: \n%s \n\n\nThe following columns are duplicate (Pair): \n%s\n\n\nNumber of duplicate rows:\n%s\n\nPercentage of duplicate rows: \n%s\n\n\nThe following rows are same: \n%s \n\n\nEmpty Columns:  \n%s \n\n\nEmpty Rows: \n%s \n\n\nThe following are same column values (particular one column): \n%s\n\nMissing Header: \n%s\n\n""" % (col_row_num_str, column_nan_value_count_str, column_zero_value_count_str, author_str, license_str, readme_str, Object_list, Int_list, Float_list, missing_cell_info_str, non_value_str, column_unique_str, row_unique_count_str, row_unique_count_percentage_str, row_unique_str, column_empty_str, row_empty_str, column_same_value_str, missing_header_str)
    print(body)

if __name__ == "__main__":
    main()
