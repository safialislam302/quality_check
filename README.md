# Quality Check (CSV files)

## Install Libraries
<b> pip install -r requirements.txt </b>


## Program run
In the <b>Upload</b> folder there are two files. 

1) <b> File.txt </b>:
Write the csv file name which you want to check. 

2) <b> Email.txt </b>:
Write the email id whom you want to send the report. 

Then upload the csv files in the upload folder and program will automatic check the files and send the email to the corresponding persons.  

## Code Details
Now this only check the csv file. It will check a lot of varities things. 
 * Authors file is present or not.
 * Readme file is present or not.
 * License file is present or not. 
 * Row and column number.
 * Missing values.
 * Zero values.
 * Header missing.
 * Type check.
 * Duplicate column check.
 * Duplicate row check. 
 * Full empty column and row check.
 * Same column values check. 

